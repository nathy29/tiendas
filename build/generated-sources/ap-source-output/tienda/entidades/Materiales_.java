package tienda.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import tienda.entidades.Disponibilidad;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-22T00:54:10")
@StaticMetamodel(Materiales.class)
public class Materiales_ { 

    public static volatile SingularAttribute<Materiales, String> nombreMateriales;
    public static volatile CollectionAttribute<Materiales, Disponibilidad> disponibilidadCollection;
    public static volatile SingularAttribute<Materiales, Integer> idMateriales;

}