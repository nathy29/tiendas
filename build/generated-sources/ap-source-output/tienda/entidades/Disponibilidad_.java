package tienda.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import tienda.entidades.Materiales;
import tienda.entidades.Tienda;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-22T00:54:10")
@StaticMetamodel(Disponibilidad.class)
public class Disponibilidad_ { 

    public static volatile SingularAttribute<Disponibilidad, Integer> cantidadDis;
    public static volatile SingularAttribute<Disponibilidad, Materiales> idMateriales;
    public static volatile SingularAttribute<Disponibilidad, Integer> idDis;
    public static volatile SingularAttribute<Disponibilidad, Tienda> idTienda;

}