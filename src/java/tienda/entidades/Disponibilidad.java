/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tienda.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Naty
 */
@Entity
@Table(name = "disponibilidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Disponibilidad.findAll", query = "SELECT d FROM Disponibilidad d")
    , @NamedQuery(name = "Disponibilidad.findByIdDis", query = "SELECT d FROM Disponibilidad d WHERE d.idDis = :idDis")
    , @NamedQuery(name = "Disponibilidad.findByCantidadDis", query = "SELECT d FROM Disponibilidad d WHERE d.cantidadDis = :cantidadDis")})
public class Disponibilidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_dis")
    private Integer idDis;
    @Column(name = "cantidad_dis")
    private Integer cantidadDis;
    @JoinColumn(name = "id_materiales", referencedColumnName = "id_materiales")
    @ManyToOne
    private Materiales idMateriales;
    @JoinColumn(name = "id_tienda", referencedColumnName = "id_tienda")
    @ManyToOne
    private Tienda idTienda;

    public Disponibilidad() {
    }

    public Disponibilidad(Integer idDis) {
        this.idDis = idDis;
    }

    public Integer getIdDis() {
        return idDis;
    }

    public void setIdDis(Integer idDis) {
        this.idDis = idDis;
    }

    public Integer getCantidadDis() {
        return cantidadDis;
    }

    public void setCantidadDis(Integer cantidadDis) {
        this.cantidadDis = cantidadDis;
    }

    public Materiales getIdMateriales() {
        return idMateriales;
    }

    public void setIdMateriales(Materiales idMateriales) {
        this.idMateriales = idMateriales;
    }

    public Tienda getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(Tienda idTienda) {
        this.idTienda = idTienda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDis != null ? idDis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Disponibilidad)) {
            return false;
        }
        Disponibilidad other = (Disponibilidad) object;
        if ((this.idDis == null && other.idDis != null) || (this.idDis != null && !this.idDis.equals(other.idDis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "tienda.entidades.Disponibilidad[ idDis=" + idDis + " ]";
    }
    
}
