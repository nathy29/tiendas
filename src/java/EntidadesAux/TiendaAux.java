/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntidadesAux;

import tienda.entidades.Tienda;

/**
 *
 * @author Naty 
 */
public class TiendaAux {
    private Tienda tienda;

    public TiendaAux() {
       tienda=new Tienda();
    }

    public TiendaAux(Tienda tienda) {
        this.tienda = tienda;
    }

    
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
    
    
}
