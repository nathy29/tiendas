/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntidadesAux;

import tienda.entidades.Materiales;

/**
 *
 * @author Naty
 */
public class MaterialesAux {
    private Materiales materiales;

    public MaterialesAux() {
        materiales=new Materiales();
    }

    public MaterialesAux(Materiales materiales) {
        this.materiales = materiales;
    }

    public Materiales getMateriales() {
        return materiales;
    }

    public void setMateriales(Materiales materiales) {
        this.materiales = materiales;
    }
    
    
    
    
    
}
