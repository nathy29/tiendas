/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import EntidadesAux.MaterialesAux;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import tienda.entidades.Materiales;
import tienda.entidades.service.MaterialesFacadeREST;

/**
 *
 * @author Naty
 */
@Named(value = "material")
@SessionScoped
public class material implements Serializable {

    @EJB
    private MaterialesFacadeREST materialesFacadeREST;

    /**
     * Creates a new instance of Materiales
     */
    
    private MaterialesAux materialesAux;
     private List<MaterialesAux> listMaterialesaaux;
     private int variableglobal;
    
    public material() {
        materialesAux=new MaterialesAux();
        variableglobal=0;
        listMaterialesaaux= new ArrayList<>();
    }

    public MaterialesFacadeREST getMaterialesFacadeREST() {
        return materialesFacadeREST;
    }

    public void setMaterialesFacadeREST(MaterialesFacadeREST materialesFacadeREST) {
        this.materialesFacadeREST = materialesFacadeREST;
    }
    
    /**
 *
 * @return Lista de materiales
 * @descripcion: este metodo permite obtener la lista de materiales
 */
    public List<Materiales> listaMateriales(){
       return materialesFacadeREST.findAll();
    }

    public List<MaterialesAux> getListMaterialesaaux() {
        
        List<Materiales> listmateriales = new ArrayList<>();
     
        
        if (variableglobal == 0) {
               listmateriales = materialesFacadeREST.findAll();
            listMaterialesaaux.clear();
                for (Materiales materiales : listmateriales) {
                    materialesAux=new MaterialesAux(materiales);
                    listMaterialesaaux.add(materialesAux);
                }
            
        }
        variableglobal = 1;
        return listMaterialesaaux;
    }

    public void setListMaterialesaaux(List<MaterialesAux> listMaterialesaaux) {
        
        
        this.listMaterialesaaux = listMaterialesaaux;
    }

   
    /**
     *
     * @param materiales
     * @descripcion: Permite actualizar o guardar los materiales
     */
    public void guardarMateriales(Materiales materiales) {
        if (materiales.getIdMateriales() == null) {
            materialesFacadeREST.create(materiales);
        } else {
           materialesFacadeREST.edit(materiales);
        }
        
         List<Materiales> listmateriales = new ArrayList<>();
     
        
      
               listmateriales = materialesFacadeREST.findAll();
            listMaterialesaaux.clear();
                for (Materiales materiales1 : listmateriales) {
                    materialesAux=new MaterialesAux(materiales1);
                    listMaterialesaaux.add(materialesAux);
                }
            
     
        variableglobal = 0;
        
    }

    /**
     *
     * @param materiales
     * @descripcion: Permite eliminar los materiales determinada o seleccionada
     */
    public void eliminarMateriales(Materiales materiales) {
        if (materiales.getIdMateriales() == null) {

        } else {
           materialesFacadeREST.remove(materiales);
        }
        variableglobal = 0;
    }
    
    /**
     *
     * @descripcion:agregar una nueva fila para agregar un material en la base de datos
     */
    public void agregarTienda() {
      
        List<Materiales> listmateriales = new ArrayList<>();
     
        
      
               listmateriales = materialesFacadeREST.findAll();
            listMaterialesaaux.clear();
                for (Materiales materiales1 : listmateriales) {
                    materialesAux=new MaterialesAux(materiales1);
                    listMaterialesaaux.add(materialesAux);
                }
            
     
                materialesAux=new MaterialesAux(new Materiales());
                //tiendaAux.setTienda(new Tienda());
                listMaterialesaaux.add(materialesAux);
                
                //variableglobaltienda = 0;
    }
}
