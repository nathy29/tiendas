/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import EntidadesAux.TiendaAux;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import tienda.entidades.Tienda;
import tienda.entidades.service.TiendaFacadeREST;

/**
 *
 * @author Naty
 */
@Named(value = "tienda")
@SessionScoped
public class tienda implements Serializable {

    @EJB
    private TiendaFacadeREST tiendaFacadeREST;

    private TiendaAux tiendaAux;

    private List<TiendaAux> listTiendaaux;
    private int variableglobaltienda;

    /**
     * Creates a new instance of tienda
     */
    public tienda() {
        tiendaAux = new TiendaAux();
        listTiendaaux = new ArrayList<>();
        variableglobaltienda = 0;
    }

    /**
     *
     * @return Lista de tiendas
     * @descripcion: este metodo permite obtener la lista de tiendas
     */
    public List<Tienda> listaTienda() {
        return tiendaFacadeREST.findAll();
    }

    public TiendaFacadeREST getTiendaFacadeREST() {
        return tiendaFacadeREST;
    }

    public void setTiendaFacadeREST(TiendaFacadeREST tiendaFacadeREST) {
        this.tiendaFacadeREST = tiendaFacadeREST;
    }

    public List<TiendaAux> getListTiendaaux() {
        List<Tienda> listiendas = new ArrayList<>();
     
        
        if (variableglobaltienda == 0) {
               listiendas = tiendaFacadeREST.findAll();
            listTiendaaux.clear();
                for (Tienda tienda : listiendas) {
                    tiendaAux=new TiendaAux(tienda);
                    listTiendaaux.add(tiendaAux);
                }
            
        }
        variableglobaltienda = 1;
        return listTiendaaux;
    }

    public void setListTiendaaux(List<TiendaAux> listTiendaaux) {
        this.listTiendaaux = listTiendaaux;
    }

    /**
     *
     * @param tienda
     * @descripcion: Permite actualizar o guardar la tienda
     */
    public void guardarTienda(Tienda tienda) {
        if (tienda.getIdTienda() == null) {
            tiendaFacadeREST.create(tienda);
        } else {
            tiendaFacadeREST.edit(tienda);
        }
        
         List<Tienda> listiendas = new ArrayList<>();
        listiendas = tiendaFacadeREST.findAll();
        listTiendaaux.clear();
            
                for (Tienda tiendas : listiendas) {
                     tiendaAux=new TiendaAux(tiendas);
                    listTiendaaux.add(tiendaAux);
                }
            variableglobaltienda = 0;
        
    }

    /**
     *
     * @param tienda
     * @descripcion: Permite eliminar la tienda determinada o seleccionada
     */
    public void eliminarTienda(Tienda tienda) {
        if (tienda.getIdTienda() == null) {

        } else {
            tiendaFacadeREST.remove(tienda);
        }
        variableglobaltienda = 0;
    }
    
    /**
     *
     * @descripcion:agregar una nueva fila para agregar una tienda en la base de datos
     */
    public void agregarTienda() {
      
         List<Tienda> listiendas = new ArrayList<>();
        listiendas = tiendaFacadeREST.findAll();
        listTiendaaux.clear();
            
                for (Tienda tiendas : listiendas) {
                     tiendaAux=new TiendaAux(tiendas);
                    listTiendaaux.add(tiendaAux);
                } 
                tiendaAux=new TiendaAux(new Tienda());
                //tiendaAux.setTienda(new Tienda());
                listTiendaaux.add(tiendaAux);
                
                //variableglobaltienda = 0;
    }

}
